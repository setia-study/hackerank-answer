function isValid(s: string): string {
  const wordFrequency = frequency(s.split(""))
  const wordFrequencyValues: number[] = Object.values(wordFrequency)
  // Rule Out Outright Impossible Cases (There exist min and max different from the most frequent)
  // This is Band Aid solution lol as it need to remap frequency (will cause problem on large array)
  const countFrequency = frequency(wordFrequencyValues)
  if (Object.values(countFrequency).length > 2) return "NO"
  // 
  const min = Math.min(...wordFrequencyValues)
  const max = Math.max(...wordFrequencyValues)
  const minCount = wordFrequencyValues.filter(v => v === min).length
  const maxCount = wordFrequencyValues.filter(v => v === max).length
  if (min === max) return "YES"
  if (minCount === 1 && min === 1) return "YES"
  if (maxCount === 1 && max - min === 1) return "YES"

  return "NO"
}

function frequency(arr: any[]) {
  const obj: any = {}
  arr.forEach(e => {
    if (obj[e]) {
      obj[e]++
    } else obj[e] = 1
  })
  return obj
}