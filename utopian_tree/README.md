# Utopian Tree
- Problem Link:
https://www.hackerrank.com/challenges/utopian-tree/problem
- Difficulty: Easy
## Solution
if current period is even, next period is current value * 2
if current period is odd, next period is current value + 1