/**
 * 
 * @param {Array<number>} arr 
 * @returns {number}
 */
function migratoryBirds(arr) {
  let count = 0;
  let max = 0;
  let result = 0;
  arr.sort((a, b) => a - b);
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === arr[i + 1]) {
      count++;
    } else {
      count = 0;
    }
    if (count > max) {
      max = count;
      result = arr[i];
    }
  }
  return result;
}