const case1 = {
  s: "aba",
  n: 10,
  expectedResult: 7
}

const case2 = {
  s: "a",
  n: 1000000000000,
  expectedResult: 1000000000000
}

const case3 = {
  s: "abcac",
  n: 10,
  expectedResult: 4
}

function repeatedString(s: string, n: number): number {
  // Write your code here
  const acount = countA(s)
  const nmod = n % s.length

  if (nmod === 0) {
      return n / s.length * acount
  }

  const left = Math.floor(n / s.length) * acount
  const remaining = s.split("").slice(0, nmod).join()
  const right = countA(remaining)
  return left + right
}

function countA(s: string) {
  let count = 0;
  s.toLowerCase().split("").forEach((e) => {
      if (e === "a") count++
  })
  return count
}

console.log("Result:", repeatedString(case1.s, case1.n), "Expected:", case1.expectedResult)
console.log("Result:", repeatedString(case2.s, case2.n), "Expected:", case2.expectedResult)
console.log("Result:", repeatedString(case3.s, case3.n), "Expected:", case3.expectedResult)