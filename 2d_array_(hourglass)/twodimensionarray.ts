function hourglassSum(arr: number[][]): number {
  const allSums: number[] = []
  for (let i = 0; i < arr.length - 2; i++) {
    for (let j = 0; j < arr.length - 2; j++) {
      const sum = subHour(i, j, arr)
      allSums.push(sum)
    }
  }
  return Math.max(...allSums)
}

function subHour(i: number, j: number, arr: number[][]) {
  /**
   * [0][0]	[0][1]	[0][2]
   *   x	  [1][1]	  x
   * [2][0]	[2][1]	[2][2]
   */
  return (
    arr[i][j] + arr[i][j + 1] + arr[i][j + 2] +
    arr[i + 1][j + 1] +
    arr[i + 2][j] + arr[i + 2][j + 1] + arr[i + 2][j + 2]
  )
}