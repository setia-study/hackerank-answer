function makeAnagram(a: string, b: string): number {
  const arrA = a.split("")
  const arrB = b.split("")

  let doubles = 0;

  arrA.forEach(w => {
      let indexinB = arrB.indexOf(w)
      if (arrB.indexOf(w) >= 0) {
          doubles++
          arrB.splice(indexinB, 1)
      }
  })

  return arrA.length - doubles + arrB.length
}