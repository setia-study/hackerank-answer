/*
 * Complete the 'extraLongFactorials' function below.
 *
 * The function accepts INTEGER n as parameter.
 */

function extraLongFactorials(n) {
  const memo = {};
  function factorial() {
    if (n === 1) return 1;
    if (memo[n]) return memo[n];
    memo[n] = n * factorial(n - 1);
    return memo[n];
  }
  return factorial(n);
}