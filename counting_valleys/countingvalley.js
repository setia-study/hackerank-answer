/*
 * Complete the 'countingValleys' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER steps
 *  2. STRING path
 */

/**
 * 
 * @param {number} steps 
 * @param {string} path 
 */
function countingValleys(steps, path) {
  const p = {
    'U': 1,
    'D': -1
  };
  let start = 0;
  const sv = path.split("").map((e) => {
    start += p[e];
    return start;
  });
  let valleys = 0;
  sv.forEach((v, i, a) => {
    if (v === 0 && a[i - 1] < 0) {
      valleys++;
    }
  });
  return valleys;
}