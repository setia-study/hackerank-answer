# Counting Valley
- Problem Link:
https://www.hackerrank.com/challenges/counting-valleys/problem
- Difficulty: Easy

## Solution
Since one valley is a sequence of steps below sea level and then back to sea level
we only need to count the number of times where we are at level and the previous step is below sea level.