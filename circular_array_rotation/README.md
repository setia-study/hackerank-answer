# Circular Array Rotation
- Problem Link:
https://www.hackerrank.com/challenges/circular-array-rotation/problem
- Difficulty: Easy
## Solution
Simply rotate the array k times and return the element at index q.